package cn.edu.dgut.sw.security.oauth2.sample;

import cn.edu.dgut.css.sai.security.oauth2.config.SaiOAuth2LoginSecurityConfigurer;
import cn.edu.dgut.css.sai.security.oauth2.config.annotation.EnableSaiOAuth2Login;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 使用例子
 *
 * @author Sai
 * @see WebSecurityConfigurerAdapter
 * @see EnableSaiOAuth2Login
 * @since 1.0
 */
@EnableSaiOAuth2Login
@Configuration
@Slf4j
public class SaiOAuth2LoginConfiguration extends WebSecurityConfigurerAdapter {

    // 设置security的拦截规则 START
    @Override
    public void configure(WebSecurity web) {
        // 不能在这个方法里忽略登录login的路径
        // 否则security的filter都不匹配login路径造成不能登录
        // 在这里忽略掉静态资源的路径是比较好的选择
        // 这与在配置文件设置security.ignoring是一样的
        web.ignoring().antMatchers("/css/**", "/js/**", "/fonts/**", "/images/**", "/favicon.ico", "/webjars/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .apply(new SaiOAuth2LoginSecurityConfigurer())
                    .and()
                .oauth2Login().loginPage("/login").permitAll()
//                                .defaultSuccessUrl("/",true)
                                .successHandler((request, response, authentication) -> {
                                    // 一般登录成功后，我们会根据业务要求作一些处理。我们可以定义successHandler完成这个需求。
                                    // 当我们定义了 successHandler 后，defaultSuccessUrl会失效。
                                    // 注意，authentication 变量的实际类型是 OAuth2AuthenticationToken 。
                                    // 同时，SecurityContextHolder.getContext().getAuthentication() 也引用了这个对象，这里只是作示例提醒一下。
                                    log.info("SecurityContextHolder.getContext().getAuthentication() = " + SecurityContextHolder.getContext().getAuthentication());
                                    log.info("authentication = " + authentication);
                                    response.sendRedirect("/");
                                })
        ;
        // @formatter:on
    }
}
